<?php

namespace Database\Seeders;

use App\Models\Clients;
use App\Models\Products;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Clients::factory()
            ->has(Products::factory()->count(3))
            ->count(10)
            ->create();
        // \App\Models\User::factory(10)->create();
    }
}
